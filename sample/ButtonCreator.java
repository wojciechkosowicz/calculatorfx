package sample;

import com.sun.javafx.geom.BaseBounds;
import com.sun.javafx.geom.transform.BaseTransform;
import com.sun.javafx.jmx.MXNodeAlgorithm;
import com.sun.javafx.jmx.MXNodeAlgorithmContext;
import com.sun.javafx.sg.prism.NGNode;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;

import java.util.HashMap;

/**
 * Created by wojtek on 24.12.15.
 */
public class ButtonCreator {
    protected GridPane layout = new GridPane();
    protected TextField workingTextField;
    protected HashMap<String, Button> buttonMap = new HashMap<String, Button>();

    public TextField textField() {
        return workingTextField;
    }

    public void setWorkingTextField(TextField field) {
        workingTextField = field;
    }

    public GridPane layout() {
        return layout;
    }

    public Button addCustomButton(String text, int row, int column, EventHandler<ActionEvent> handler) {
        Button button = new Button(text);
        layout.add(button, row, column);
        button.setOnAction(handler);
        return button;
    }


    public HashMap<String, Button> buttonMap() {
        return buttonMap;
    }

    public Button buttonWithText(String text) {
        return buttonMap.get(text);
    }

    public void addButton(String text, int row, int column) {
        Button button = new Button(text);
        layout.add(button, row, column);
        buttonMap.put(text, button);
    }
}

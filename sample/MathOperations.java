/**
 * Created by wojtek on 07.12.15.
 */
package sample;

public class MathOperations {
    private static MathOperations instance = null;
    private boolean inMiddleOfOperation = false;
    private Double result = new Double(0);
    private OperationMath operation = OperationMath.NONE;

    private MathOperations() {
    }

    public static MathOperations getInstance() {
        if (instance == null)
            instance = new MathOperations();
        return instance;
    }

    public void setMiddleOperation(boolean value) {
        inMiddleOfOperation = value;
    }

    public boolean middleOperation(boolean value) {
        return inMiddleOfOperation;
    }

    public void setOpMath(OperationMath op) {
        operation = op;
    }

    public OperationMath getOperationMath() {
        return operation;
    }

    public void doMath(OperationMath op, double number) {
        if (operation == OperationMath.NONE) {
            result = number;
            operation = op;
            return;
        }
        operation = op;
        PerformMathOperations(op, number);
    }

    public Double getResult() {
        operation = OperationMath.NONE;
        return result;
    }

    public void doSumOperation(OperationMath operation, double number) {
        PerformMathOperations(operation, number);
        operation = OperationMath.NONE;
    }

    public void reset() {
        result = 0.0;
        operation = OperationMath.NONE;
    }

    private void AddOperation(double number) {
        result += number;
    }

    private void SubtractOperation(double number) {
        result -= number;
    }

    private void MultiplyOperation(double number) {
        result *= number;
    }

    private void DivideOperation(double number) {
        result /= number;
    }

    private void SqrtOperation(double number) {
        result = Math.sqrt(number);
    }

    private void PerformMathOperations(OperationMath operation, double number) {
        switch (operation) {
            case ADD:
                AddOperation(number);
                break;
            case SUBTRACT:
                SubtractOperation(number);
                break;
            case MULTIPLY:
                MultiplyOperation(number);
                break;
            case DIVIDE:
                DivideOperation(number);
                break;
            case SQRT:
                SqrtOperation(number);
                break;
        }
    }
}

package sample;

import javafx.scene.control.Button;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

import java.util.HashMap;

/**
 * Created by wojtek on 24.12.15.
 */
public class NumberButtons extends ButtonCreator {

    public void createNumberGrid() {
        addButton("1", 1, 1);
        addButton("2", 2, 1);
        addButton("3", 3, 1);
        addButton("4", 1, 2);
        addButton("5", 2, 2);
        addButton("6", 3, 2);
        addButton("7", 1, 3);
        addButton("8", 2, 3);
        addButton("9", 3, 3);
        addButton("0", 2, 4);
        setClickActions();
        prepareMissingButtons();
    }

    @Override
    public void addButton(String text, int row, int column) {
        super.addButton(text, row, column);
    }

    private void setClickActions() {
        for (final HashMap.Entry<String, Button> entry : buttonMap.entrySet()) {
            entry.getValue().setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    workingTextField.setText(workingTextField.getText() + entry.getKey());
                }
            });
        }
    }

    public void prepareMissingButtons() {
        addCustomButton(".", 1, 4, new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

            }
        });
        addCustomButton("-", 3, 4, new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
            }
        });


    }

}

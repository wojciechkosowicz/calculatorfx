package sample;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;

/**
 * Created by wojtek on 24.12.15.
 */
public class OperationButtons extends ButtonCreator {
    MathOperations mathOperations = MathOperations.getInstance();

    public void createAllOperationButtons() {
        createBasicOperationButtons();
        addCustomButton("=", 2, 3, new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                mathOperations.doSumOperation(mathOperations.getOperationMath(), Double.parseDouble(workingTextField.getText()));
                workingTextField.setText(mathOperations.getResult().toString());
            }
        });
        addCustomButton("CE", 1, 3, new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                mathOperations.reset();
                workingTextField.setText("");
            }
        });
    }

    public void createBasicOperationButtons() {
        addCustomButton("+", 1, 1, new OperationActionEvent(OperationMath.ADD, workingTextField));
        addCustomButton("-", 1, 2, new OperationActionEvent(OperationMath.SUBTRACT, workingTextField));
        addCustomButton("*", 2, 1, new OperationActionEvent(OperationMath.MULTIPLY, workingTextField));
        addCustomButton("/", 2, 2, new OperationActionEvent(OperationMath.DIVIDE, workingTextField));
    }



}

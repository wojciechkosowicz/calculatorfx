package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Main extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        VBox mainLayout = new VBox();
        TextField result = new TextField("");
        result.setAlignment(Pos.BOTTOM_RIGHT);
        NumberButtons numberButtons = new NumberButtons();
        numberButtons.setWorkingTextField(result);
        numberButtons.createNumberGrid();
        mainLayout.getChildren().add(result);
        HBox subLayout = new HBox();
        mainLayout.getChildren().add(subLayout);
        subLayout.getChildren().add(numberButtons.layout());
        OperationButtons operationButtons = new OperationButtons();
        operationButtons.setWorkingTextField(result);
        operationButtons.createAllOperationButtons();
        subLayout.getChildren().add(operationButtons.layout());
        primaryStage.setScene(new Scene(mainLayout, 300, 275));
        primaryStage.show();
    }
}

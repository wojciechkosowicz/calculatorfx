package sample;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;
public class OperationActionEvent implements EventHandler<ActionEvent> {
    private TextField field;
    private OperationMath operation;
    private MathOperations mathInstance = MathOperations.getInstance();
    public OperationActionEvent(OperationMath op, TextField field)  {
        operation = op;
        this.field = field;

    }

    @Override
    public void handle(ActionEvent event) {
        mathInstance.doMath(operation, Double.parseDouble(field.getText()));
        field.setText("");
    }
}
